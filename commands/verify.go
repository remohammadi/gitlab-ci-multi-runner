package commands

import (
	"github.com/codegangsta/cli"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/gitlab-org/gitlab-ci-multi-runner/common"
	"gitlab.com/gitlab-org/gitlab-ci-multi-runner/network"
)

type VerifyCommand struct {
	configOptions
	network common.Network

	DeleteNonExisting bool `long:"delete" description:"Delete no longer existing runners?"`
}

func (c *VerifyCommand) Execute(context *cli.Context) {
	userModeWarning(true)

	err := c.loadConfig()
	if err != nil {
		log.Fatalln(err)
		return
	}

	// verify if runner exist
	runners := []*common.RunnerConfig{}
	for _, runner := range c.config.Runners {
		if c.network.VerifyRunner(runner.RunnerCredentials) {
			runners = append(runners, runner)
		} else if c.DeleteNonExisting {
			err := c.config.RemoveRunner(&runner.RunnerCredentials)
			if err != nil {
				log.Fatalln("Failed to remove the runner", c.ConfigFile, err)
				return
			}
			log.Println("Runner is removed from the config file:", runner.RunnerCredentials.UniqueID())
		}
	}
}

func init() {
	common.RegisterCommandByDetails("verify", "verify all registered runners", &VerifyCommand{
		network: &network.GitLabClient{},
	})
}
