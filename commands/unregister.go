package commands

import (
	"github.com/codegangsta/cli"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/gitlab-org/gitlab-ci-multi-runner/common"
	"gitlab.com/gitlab-org/gitlab-ci-multi-runner/network"
)

type UnregisterCommand struct {
	configOptions
	network common.Network
	Name    string `toml:"name" json:"name" short:"n" long:"name" description:"Name of the runner you wish to unregister"`
}

func (c *UnregisterCommand) Execute(context *cli.Context) {
	userModeWarning(false)

	err := c.loadConfig()
	if err != nil {
		log.Fatalln(err)
		return
	}

	if len(c.Name) == 0 {
		log.Fatalln("You should specify the name of the runner you wish to unregister")
		return
	}

	runnerConfig, err := c.RunnerByName(c.Name)
	if err != nil {
		log.Fatalln(err)
		return
	}
	runnerCredentials := runnerConfig.RunnerCredentials

	if !c.network.DeleteRunner(runnerCredentials) {
		log.Fatalln("Failed to delete runner", c.Name)
		return
	}

	err = c.config.RemoveRunner(&runnerCredentials)
	if err != nil {
		log.Fatalln("Failed to remove the runner", c.ConfigFile, err)
		return
	}

	log.Println("Updated", c.ConfigFile)
}

func init() {
	common.RegisterCommandByDetails("unregister", "unregister specific runner", &UnregisterCommand{
		network: &network.GitLabClient{},
	})
}
